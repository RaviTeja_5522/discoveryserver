# Base Image as Open jdk
FROM openjdk:8-jre

#Copying the jar file into target

COPY target /app

# Changing the working directory

WORKDIR /app

# specifying the executable to run the container

ENTRYPOINT ["java","-jar","demo-0.0.1-SNAPSHOT.jar"]
